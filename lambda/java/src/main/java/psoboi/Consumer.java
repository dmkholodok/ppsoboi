package psoboi;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.GetResponse;
import psoboi.event.CreateMetricEvent;

import java.io.IOException;


public class Consumer implements RequestHandler<String, String> {

    private final static String QUEUE_NAME = "metrics";
    private final static String TABLE_NAME = "metrics";
    private final static String TABLE_KEY_NAME = "name";


    @Override
    public String handleRequest(String event, Context context) {
        String msg = pullMessage();
        if (msg ==  null) {
            return "OK";
        }

        CreateMetricEvent consumedEvent = toCreateMetricEvent(msg);
        Table table = TableProvider.getDefault().getTable(TABLE_NAME);
        Item item = new Item()
                .withPrimaryKey(TABLE_KEY_NAME, consumedEvent.getName())
                .withString("body", consumedEvent.getBody());
        table.putItem(item);

        return "OK";
    }

    private CreateMetricEvent toCreateMetricEvent(String msg) {
        String[] strings = msg.split(".+ .+");
        return new CreateMetricEvent(strings[0], strings[1]);
    }

    private String pullMessage() {
        Channel channel = MqChannelProvider.getDefault().get();
        boolean autoAck = false;
        GetResponse response;
        try {
            response = channel.basicGet(QUEUE_NAME, autoAck);
            if (response == null) {
                return null;
            } else {
                byte[] body = response.getBody();
                long deliveryTag = response.getEnvelope().getDeliveryTag();
                String msg = new String(body);
                channel.basicAck(deliveryTag, false);

                return msg;
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
