package psoboi;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class MqChannelProvider {

    public static MqChannelProvider getDefault() {
        return new MqChannelProvider();
    }

    public Channel get() {
        try {
            Connection connection = createConnectionFactory().newConnection();
            return connection.createChannel();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private ConnectionFactory createConnectionFactory() {
        try {
            ConnectionFactory factory = new ConnectionFactory();
            String username = System.getProperty("mq.username");
            String pass = System.getProperty("mq.pass");
            String url = System.getProperty("mq.url");
            String port = System.getProperty("mq.port");
            String uriTemplate = "amqps://%s:%s@%s:%s";
            factory.setUri(String.format(uriTemplate, username, pass, url, port));
            return factory;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
