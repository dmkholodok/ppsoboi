package psoboi;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.rabbitmq.client.Channel;
import psoboi.event.CreateMetricEvent;

import java.io.IOException;

public class Producer implements RequestHandler<CreateMetricEvent, String> {

    private final static String QUEUE_NAME = "metrics";

    @Override
    public String handleRequest(CreateMetricEvent event, Context context) {
        try {
            Channel channel = MqChannelProvider.getDefault().get();
            channel.queueDeclarePassive(QUEUE_NAME);
            channel.basicPublish(
                    "",
                    QUEUE_NAME,
                    null,
                    event.toString().getBytes()
            );
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return "OK";
    }
}
