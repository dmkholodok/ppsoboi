package psoboi;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;

public class TableProvider {

    public static TableProvider getDefault() {
        return new TableProvider();
    }

    public Table getTable(String name) {
        AmazonDynamoDB client = AmazonDynamoDBClientBuilder.defaultClient();
        DynamoDB db = new DynamoDB(client);
        return db.getTable(name);
    }
}
