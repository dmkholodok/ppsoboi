package psoboi.event;

public class CreateMetricEvent {

    private String name;
    private String body;

    public CreateMetricEvent(String name, String body) {
        this.name = name;
        this.body = body;
    }

    public CreateMetricEvent() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return name + " " + body;
    }
}
