import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.document.Table
import com.rabbitmq.client.Channel
import com.rabbitmq.client.Envelope
import com.rabbitmq.client.GetResponse
import org.junit.runner.RunWith
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.spockframework.runtime.Sputnik
import psoboi.Consumer
import psoboi.MqChannelProvider
import psoboi.TableProvider
import spock.lang.Specification
import spock.lang.Subject

import static org.mockito.Mockito.when
import static org.powermock.api.mockito.PowerMockito.mockStatic

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(Sputnik.class)
@PrepareForTest([MqChannelProvider.class, TableProvider.class])
class ConsumerSpec extends Specification {

    @Subject
    def consumer = new Consumer()

    def "setup"() {
        mockStatic(MqChannelProvider.class)
        mockStatic(TableProvider.class)
    }

    def "should return OK if nullable message is returned"() {
        given:
            def channel = Mock(Channel)
            def mockChannelProvider = Mock(MqChannelProvider)
            when(MqChannelProvider.getDefault()).thenReturn(mockChannelProvider)

            1 * mockChannelProvider.get() >> channel
            1 * channel.basicGet("metrics", false) >>  null
            0 * _

        when:
            def res = consumer.handleRequest(null, null)

        then:
            res == "OK"
    }

    def "should return OK and put a message from queue into the database"() {
        given:
            def channel = Mock(Channel)
            def mockChannelProvider = Mock(MqChannelProvider)
            def mockTableProvider = Mock(TableProvider)
            def mockTable = Mock(Table)
            when(MqChannelProvider.getDefault()).thenReturn(mockChannelProvider)
            when(TableProvider.getDefault()).thenReturn(mockTableProvider)

            1 * mockChannelProvider.get() >> channel
            1 * channel.basicGet("metrics", false) >> new GetResponse(
                    new Envelope(1, false, null, null),
                    null,
                    "name body".bytes,
                    1
            )
            1 * mockTableProvider.getTable("metrics") >> mockTable
            1 * channel.basicAck(1, false)
            1 * mockTable.putItem(_ as Item)
            0 * _

        when:
            def res = consumer.handleRequest(null, null)

        then:
            res == "OK"
    }
}
