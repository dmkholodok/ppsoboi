import com.rabbitmq.client.Channel
import org.junit.runner.RunWith
import org.powermock.core.classloader.annotations.PrepareForTest
import org.powermock.modules.junit4.PowerMockRunner
import org.powermock.modules.junit4.PowerMockRunnerDelegate
import org.spockframework.runtime.Sputnik
import psoboi.MqChannelProvider
import psoboi.Producer
import psoboi.event.CreateMetricEvent
import spock.lang.Specification
import spock.lang.Subject

import static org.mockito.Mockito.when
import static org.powermock.api.mockito.PowerMockito.mockStatic

@RunWith(PowerMockRunner.class)
@PowerMockRunnerDelegate(Sputnik.class)
@PrepareForTest([MqChannelProvider.class])
class ProducerSpec extends Specification {

    @Subject
    def producer = new Producer()

    def "setup"() {
        mockStatic(MqChannelProvider.class)
    }

    def "should publish message to queue"() {
        given:
            def channel = Mock(Channel)
            def mockChannelProvider = Mock(MqChannelProvider)
            when(MqChannelProvider.getDefault()).thenReturn(mockChannelProvider)

            1 * mockChannelProvider.get() >> channel
            1 * channel.queueDeclarePassive("metrics")
            1 * channel.basicPublish("", "metrics", null, _ as byte[])
            0 * _

        when:
            def res = producer.handleRequest(new CreateMetricEvent(), null)

        then:
            res == "OK"
    }

    def "should throw IOException wrapped in RuntimeException"() {
        given:
            def channel = Mock(Channel)
            def mockChannelProvider = Mock(MqChannelProvider)
            when(MqChannelProvider.getDefault()).thenReturn(mockChannelProvider)

            1 * mockChannelProvider.get() >> channel
            1 * channel.queueDeclarePassive("metrics") >> { throw new IOException("msg") }

        when:
            producer.handleRequest(new CreateMetricEvent(), null)

        then:
            def exc = thrown(RuntimeException)
            exc.cause.message.is("msg")
            exc.cause.class.is(IOException.class)
    }
}
