import json
import boto3

bucket_name = "s3-bucket-psoboi"


def lambda_handler(event, context):
    table_name = event['table_name']
    last_evaluated_key = None
    while True:
        items, last_evaluated_key = get_items(table_name, last_evaluated_key)
        for item in items:
            put_item(item)

        if not last_evaluated_key:
            break

    return "OK"


def get_items(table_name, last_evaluated_key):
    db_client = boto3.client('dynamodb')
    if last_evaluated_key:
        response = db_client.scan(
            TableName=table_name,
            ExclusiveStartKey=last_evaluated_key
        )
    else:
        response = db_client.scan(TableName=table_name)
    return response['Items'], response['LastEvaluatedKey']


def put_item(item):
    s = json.dumps(item['body']['S'])
    arr = bytes(s, 'utf-8')
    file_name = 'metrics/' + item['name']['S'] + '.json'
    bucket = get_bucket(bucket_name)
    bucket.put_object(Key=file_name, Body=arr)


def get_bucket(bucket_name):
    s3 = boto3.resource("s3")
    return s3.Bucket(bucket_name)