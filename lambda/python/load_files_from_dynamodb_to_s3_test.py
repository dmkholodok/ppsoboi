import unittest
from unittest.mock import Mock
from unittest.mock import patch
from unittest.mock import ANY

import load_files_from_dynamodb_to_s3 as src
from test_data import test_item


class LambdaHandlerTest(unittest.TestCase):

    def test_return_no_items_from_db(self):
        src.get_items = Mock(return_value=([], None))
        self.assertTrue(src.lambda_handler({'table_name': 'XXX'}, None), 'OK')

    @patch('impl_file.put_item')
    def test_return_some_items_from_db(self, mock_put_item):
        src.get_items = Mock(return_value=([test_item], None))

        self.assertTrue(src.lambda_handler({'table_name': 'XXX'}, None), 'OK')
        mock_put_item.assert_called_with(test_item)

    @patch('impl_file.get_bucket')
    def test_put_item(self, mock_get_bucket):
        mock_bucket = Mock()
        mock_bucket.put_object = Mock()
        mock_get_bucket.return_value = mock_bucket

        src.put_item(test_item)

        mock_get_bucket.assert_called_with('s3-bucket-psoboi')
        mock_bucket.put_object.assert_called_once_with(Key='metrics/name_value.json', Body=ANY)

    @patch('boto3.client')
    def test_get_items_with_none_last_eval_key(self, get_mock_client):
        db_client = Mock()
        get_mock_client.return_value = db_client
        db_client.scan = Mock(return_value={'Items': [], 'LastEvaluatedKey': None})

        items, last_eval_key = src.get_items('tn', None)

        db_client.scan.assert_called_once_with(TableName='tn')
        self.assertEqual(items, [])
        self.assertEqual(last_eval_key, None)

    @patch('boto3.client')
    def test_get_items_with_set_last_eval_key(self, get_mock_client):
        db_client = Mock()
        get_mock_client.return_value = db_client
        db_client.scan = Mock(return_value={'Items': [test_item], 'LastEvaluatedKey': None})

        items, last_eval_key = src.get_items('tn', 'Dmitry')

        db_client.scan.assert_called_once_with(TableName='tn', ExclusiveStartKey='Dmitry')
        self.assertEqual(items, [test_item])
        self.assertEqual(last_eval_key, None)


if __name__ == '__main__':
    unittest.main()
