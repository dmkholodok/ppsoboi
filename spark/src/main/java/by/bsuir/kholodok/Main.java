package by.bsuir.kholodok;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.mapping;

public class Main {

    private static final String RES_FILENAME = "res.txt";

    public static void main(String[] args) {
        SparkSession sparkSession = SparkSession
                .builder()
                .appName("SparkJob")
                .getOrCreate();

        String s3Folder = args[0];
        System.out.println("Reading from " + s3Folder);
        File dir = new File(s3Folder);
        File[] files = dir.listFiles((d, name) -> name.endsWith(".json"));
        Map<String, String> res = Stream.of(files)
                .map(File::getAbsolutePath)
                .map(filename -> sparkSession.read().option("multiLine", true).json(filename))
                .map(ds -> processDs(sparkSession, ds))
                .collect(groupingBy(it -> it._1, mapping(it -> it._2, joining(", ", "{", "}"))));
        writeToFile(s3Folder + RES_FILENAME, res);
    }

    private static Tuple2<String, String> processDs(SparkSession sparkSession, Dataset<Row> ds) {
        String metricName = ds.schema().fieldNames()[1];
        ds.createOrReplaceTempView("temp");
        Row maxRow = sparkSession.sql("SELECT * FROM temp ORDER BY " + metricName + " DESC LIMIT 1").first();
        return new Tuple2<>(maxRow.getString(0), metricName + " = " + maxRow.get(1));
    }

    private static void writeToFile(String path, Map<String, String> body) {
        try {
            Files.write(Paths.get(path), body.toString().getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}